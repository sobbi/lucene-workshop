package spring.startup;

import document.DocReader;
import document.WikiDoc;
import document.xml.XmlWikiDocs;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class LuceneIndexing implements InitializingBean {
    private static final Logger LOGGER = Logger.getLogger(LuceneIndexing.class.getSimpleName());
    private static final String TITLE_FIELD = "Title";
    private static final String CONTENT_FIELD = "Content";
    @Value("${index.path}")
    private String index_path;

    private IndexWriter indexWriter;
    @Autowired
    private DocReader reader;

    public LuceneIndexing() {
        LOGGER.info("Contructor Called");
    }

    @PostConstruct
    public void init() throws IOException {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        LOGGER.info("AfterPropertiesSet Called");
        File file = new File(index_path);
        if (!file.isDirectory() || file.listFiles().length > 1) {
            return;
        }
        //TODO: IndexWriter Initialisieren

        reader.getReaders().entrySet().stream().forEach(entry -> {
            int k = entry.getKey();
            List<WikiDoc> wikiDocs = new ArrayList<>();
            while (!reader.isEOF(k)) {
                List<Document> documents = new ArrayList<>();
                try {
                    XmlWikiDocs xmlWikiDocs = reader.readDoc(k, 5000);
                    if (xmlWikiDocs.getPages() != null && !xmlWikiDocs.getPages().isEmpty()) {
                        wikiDocs = xmlWikiDocs.convertToWikiDocs();
                    }
                    /*
                      TODO:
                      Generieren eine Liste von Dokumenten aus wikiDocs.
                      Ein Dokument soll aus einem Titel und dem Inhalt bestehen.
                      Die Feldernamen sind die vorgegebenen Strings TITLE_FIELD und CONTENT_FIELD
                     */

                    //TODO: Hinzufügen aller Dokumente zum indexWriter

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("End of Task for Doc: " + k);
        });
        //TODO: IndexWriter commit&close

        return;
    }

}
