package spring.rest.controller.requests;

public class SearchRequest {

    private String field;
    private String query;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return "SearchRequest{" +
                "field='" + field + '\'' +
                ", query='" + query + '\'' +
                '}';
    }
}
