package spring.rest.controller.responses;

import java.util.Objects;

public class SearchResponse {

    private String title;
    private Float score;
    private Integer id;

    public SearchResponse(String title, Float score, Integer id) {
        this.title = title;
        this.score = score;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "title='" + title + '\'' +
                ", score=" + score +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResponse that = (SearchResponse) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(score, that.score) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, score, id);
    }
}
