package spring.rest.controller;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import spring.rest.controller.requests.SearchRequest;
import spring.rest.controller.responses.SearchResponse;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RestWiki {

    @Value("${index.path}")
    private String index_path;


    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public String open(@PathVariable String id) throws IOException {
        FSDirectory indexDir = FSDirectory.open(Paths.get(index_path));
        IndexReader indexReader = DirectoryReader.open(indexDir);
        IndexSearcher searcher = new IndexSearcher(indexReader);
        Integer convertedId = Integer.valueOf(id);
        return searcher.doc(convertedId).getField("Content").stringValue();
    }

    @RequestMapping(path = "/search", method = RequestMethod.POST)
    public List<SearchResponse> search(@RequestBody SearchRequest searchRequest) throws IOException, ParseException {
        List<SearchResponse> results = new ArrayList<>(10);
        //TODO: Erstellen des IndexSearchers

        //TODO: Query erzuegen

        //TODO: Suche starten und Auswerten(Ergebnis in result reinschreiben)

        return results;
    }
}
