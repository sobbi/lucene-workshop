package document;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import document.xml.XmlWikiDocs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class DocReader {

    private Map<Integer, BufferedReader> readers;
    private Map<Integer, Boolean> isEOF;

    @Value("${docs.path}")
    private String docDirPath;

    @PostConstruct
    private void init() throws FileNotFoundException {
        int c = 0;
        this.isEOF = new ConcurrentHashMap<>();
        this.readers = new ConcurrentHashMap<>();
        File dir = new File(docDirPath);
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                this.readers.put(c, new BufferedReader(new FileReader(file)));
                this.isEOF.put(c, false);
                c++;
            }
        }
    }

    public XmlWikiDocs readDoc(int docNumber, int docCount) throws IOException {
        BufferedReader bufferedReader = this.readers.get(docNumber);
        StringBuilder sb = new StringBuilder();
        String line = bufferedReader.readLine();
        if (line.equals("<XmlWikiDocs>")) {
            line = bufferedReader.readLine();
        }
        sb.append("<XmlWikiDocs>");

        while (line != null && docCount != 0) {
            sb.append(line);
            if (line.contains("</page>")) {
                docCount--;
            }
            if (docCount != 0) {
                line = bufferedReader.readLine();
            }
        }
        if (line == null) {
            this.setEOF(docNumber, true);
        }

        sb.append("</XmlWikiDocs>");
        XmlMapper mapper = new XmlMapper();
        XmlWikiDocs result = mapper.readValue(sb.toString(), XmlWikiDocs.class);
        return result;
    }


    public String getDocDirPath() {
        return docDirPath;
    }

    public void setDocDirPath(String docDirPath) {
        this.docDirPath = docDirPath;
    }

    public boolean isEOF(int docNumber) {
        return this.isEOF.get(docNumber);
    }

    public void setEOF(int docNumber, boolean EOF) {
        this.isEOF.put(docNumber, EOF);
    }

    public Map<Integer, BufferedReader> getReaders() {
        return this.readers;
    }

    public void setReaders(Map<Integer, BufferedReader> readers) {
        this.readers = readers;
    }
}
