package document.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Revision {

    @JsonProperty("text")
    private String text;

    public Revision() {
    }

    public Revision(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Revision{" +
                "text='" + this.text + '\'' +
                '}';
    }
}
