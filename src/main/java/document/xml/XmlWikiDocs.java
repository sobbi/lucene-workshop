package document.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import document.WikiDoc;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class XmlWikiDocs {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "page")
    List<Page> pages;

    public XmlWikiDocs() {
    }

    public XmlWikiDocs(List<Page> pages) {
        this.pages = pages;
    }

    public List<WikiDoc> convertToWikiDocs() {
        List<WikiDoc> result = new ArrayList<>();
        this.pages.forEach(p -> result.add(new WikiDoc(p.getTitle(), p.getRevision().getText())));
        return result;
    }

    public List<Page> getPages() {
        return this.pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "XmlWikiDocs{" +
                "pages=" + this.pages +
                '}';
    }
}
