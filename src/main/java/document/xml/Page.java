package document.xml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Page {

    private String title;

    private Revision revision;

    public Page() {
    }

    public Page(Revision revision) {
        this.revision = revision;
    }

    public Page(String title) {
        this.title = title;
    }

    public Page(String title, Revision revision) {
        this.title = title;
        this.revision = revision;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Revision getRevision() {
        return this.revision;
    }

    public void setRevision(Revision revision) {
        this.revision = revision;
    }

    @Override
    public String toString() {
        return "Page{" +
                "title='" + this.title + '\'' +
                ", revision='" + this.revision + '\'' +
                '}';
    }
}
