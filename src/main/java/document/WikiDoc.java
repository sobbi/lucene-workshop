package document;

public class WikiDoc {

    private String title;

    private String text;

    public WikiDoc() {
    }

    public WikiDoc(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "WikiDoc{" +
                "title='" + this.title + '\'' +
                ", text='" + this.text + '\'' +
                '}';
    }
}
